// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: false,
  components:true,
  vue: {
    compilerOptions: {
      isCustomElement: (tag) => tag.includes('my-'),
    },
  },
  css: [
    'assets/styles.css'
],
app:{
  head: {
    htmlAttrs: {
      'data-theme': 'dark'
    }
  }
}
});
