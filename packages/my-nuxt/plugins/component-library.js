import { ComponentLibrary } from '~/../vue-library/lib';

export default defineNuxtPlugin(nextApp => {
   nextApp.vueApp.use(ComponentLibrary)
   // nextApp.vueApp.config.ignoredElements = [/^my-/]
})