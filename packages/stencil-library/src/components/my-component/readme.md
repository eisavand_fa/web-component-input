# my-component



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute     | Description | Type      | Default         |
| ------------- | ------------- | ----------- | --------- | --------------- |
| `class`       | `class`       |             | `string`  | `undefined`     |
| `disabled`    | `disabled`    |             | `boolean` | `undefined`     |
| `errorText`   | `error-text`  |             | `string`  | `""`            |
| `height`      | `height`      |             | `any`     | `undefined`     |
| `label`       | `label`       |             | `string`  | `undefined`     |
| `leftIcon`    | `left-icon`   |             | `string`  | `undefined`     |
| `max`         | `max`         |             | `number`  | `undefined`     |
| `maxlength`   | `maxlength`   |             | `number`  | `undefined`     |
| `min`         | `min`         |             | `number`  | `undefined`     |
| `minlength`   | `minlength`   |             | `number`  | `undefined`     |
| `name`        | `name`        |             | `string`  | `undefined`     |
| `pattern`     | `pattern`     |             | `any`     | `undefined`     |
| `placeholder` | `placeholder` |             | `string`  | `"placeholder"` |
| `rightIcon`   | `right-icon`  |             | `string`  | `undefined`     |
| `type`        | `type`        |             | `string`  | `"text"`        |
| `value`       | `value`       |             | `string`  | `undefined`     |
| `width`       | `width`       |             | `any`     | `undefined`     |


## Events

| Event         | Description | Type                  |
| ------------- | ----------- | --------------------- |
| `valueChange` |             | `CustomEvent<string>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
