import { Component, h, Prop, Event, EventEmitter } from '@stencil/core';
@Component({
  tag: 'my-input',
  styleUrl :"./my-input.css",
  shadow: false,
})
export class MyInput {
  @Prop() label: string;
  @Prop() value: string;
  @Prop() type: string = "text";
  @Prop() placeholder: string = "placeholder";
  @Prop() name: string;
  @Prop() leftIcon: string;
  @Prop() rightIcon: string;
  @Prop() max: number;
  @Prop() min: number;
  @Prop() maxlength: number;
  @Prop() minlength: number;
  @Prop() pattern: any;
  @Prop() width: any;
  @Prop() height: any;
  @Prop() class: string;
  @Prop() disabled: boolean;
  @Prop() errorText: string = "";
  @Event() valueChange: EventEmitter<string>;

  handleChange(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    this.value = inputElement.value;
    this.valueChange.emit(this.value);
  }

  render() {
    return (
    <div class="nds-d-inline-grid">
      <div class={`nds-border-radius-small nds-d-inline-flex nds-border-xsmall ${this.errorText ? 'nds-border-error' : 'nds-border-outline-variant' } nds-py-8`}>
      {this.leftIcon && <span class={`nds-self-center nds-on-surface-variant-color ${this.leftIcon} nds-ps-4`}/>}
      <input 
      class={`nds-body-large text-field__input nds-placeholder-on-surface-variant--medium ${this.errorText ? 'nds-error-color' : 'nds-on-surface-alt-color' }  nds-background-transparent nds-border-none nds-px-4`}
      type={this.type} 
      name={this.name}
      min={this.min}
      pattern={this.pattern}
      width={this.width}
      height={this.height}
      max={this.max}
      disabled={this.disabled}
      maxlength={this.maxlength}
      minlength={this.minlength}
      value={this.value} 
      placeholder={this.placeholder}
      onInput={event => this.handleChange(event)} 
      />
      {this.rightIcon && <span class={`nds-self-center nds-on-surface-variant-color ${this.rightIcon} nds-pe-4`}/>}
      </div>
      {this.errorText && <span class="nds-pt-4 nds-error-color">{this.errorText}</span>}
    </div>
    );
  }
}
