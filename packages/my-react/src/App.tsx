/* eslint-disable @typescript-eslint/no-unsafe-member-access */
// App.tsx
import { useState } from 'react';
import './App.css';
import { MyInput, defineCustomElements } from 'react-library/lib/index.ts';

void defineCustomElements()

function App() {
  const [firstName, setFirstName] = useState('')
  return (
    <div className="App nds-d-flex nds-items-center nds-flex-col nds-justify-center">
      <MyInput      
      value={firstName}
      leftIcon="nds-icon-search" 
      errorText=""
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      onInput={(e: any) => setFirstName(e.target.value)}>
      </MyInput>
      <p>firstName :{firstName}</p>
    </div>
  );
}

export default App;