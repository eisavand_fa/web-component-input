/// <reference types="react" />
import type { JSX } from 'stencil-library';
export declare const MyComponent: import("react").ForwardRefExoticComponent<JSX.MyComponent & Omit<import("react").HTMLAttributes<HTMLMyComponentElement>, "style"> & import("./react-component-lib/interfaces").StyleReactProps & import("react").RefAttributes<HTMLMyComponentElement>>;
export declare const MyInput: import("react").ForwardRefExoticComponent<JSX.MyInput & Omit<import("react").HTMLAttributes<HTMLMyInputElement>, "style"> & import("./react-component-lib/interfaces").StyleReactProps & import("react").RefAttributes<HTMLMyInputElement>>;
